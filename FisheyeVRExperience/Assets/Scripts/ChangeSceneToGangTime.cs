﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneToGangTime : MonoBehaviour
{
    private float _waitTime = 90.0f;
    private float _timePassed;

    private void Update()
    {
        _timePassed += Time.deltaTime;
        if(_timePassed >= _waitTime) 
        {
            SceneManager.LoadScene("GANG_UNLOCKED", LoadSceneMode.Single);
        }
    }
}
