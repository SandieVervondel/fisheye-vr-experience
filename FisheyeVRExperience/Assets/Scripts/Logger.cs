﻿using UnityEngine;
using System;
using System.IO;

public class Logger : MonoBehaviour
{
    private string _filePath = "";

    void Start()
    {
        _filePath = Application.persistentDataPath + "/debug_log_" +DateTime.Now.ToString("dd_MM_yy-H_mm_ss") + ".txt";
        Debug.Log("Printing to: " + _filePath);
    }

    void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }

    void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
    }

    void HandleLog(string logString, string stackTrace, LogType type)
    {
        if (!File.Exists(_filePath))
        {
            // Create a file to write to.
            using (StreamWriter sw = File.CreateText(_filePath))
            {
                sw.WriteLine(logString);
            }
        }

        // Append to existing log
        using (StreamWriter sw = File.AppendText(_filePath))
        {
            sw.WriteLine(logString);
        }
    }
}
