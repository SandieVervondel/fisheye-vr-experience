﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RaycastingStart : MonoBehaviour
{
    public Image fillImage;

    private float _waitTime = 2.0f;
    private float _timePassed;
    private readonly float _size = 100;

    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
        Debug.DrawRay(ray.origin, ray.direction * _size, Color.yellow);

        RaycastHit hit;
        if(Physics.Raycast(ray, out hit))
        {
            GameObject hitObject = hit.collider.transform.parent.gameObject;
            _timePassed += Time.deltaTime;
            if (_timePassed >= _waitTime)
            {
                SceneManager.LoadScene("GANG_LOCKED", LoadSceneMode.Single);
            }
        }
        else
        {
            _timePassed = 0f;
        }
        float fillPercentage = _timePassed / _waitTime;
        fillImage.fillAmount = fillPercentage;
    }
}
