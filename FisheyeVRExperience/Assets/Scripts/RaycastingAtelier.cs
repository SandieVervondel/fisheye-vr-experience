﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;
using UnityEngine.SceneManagement;

public class RaycastingAtelier : MonoBehaviour
{
    public MediaPlayer mediaPlayerCNC;
    public MediaPlayer mediaPlayerWood;
    public MediaPlayer mediaPlayerMetal;
    public MediaPlayer mediaPlayerPaint;
    public MediaPlayer mediaPlayerTechniek;
    public Image fillImage;

    private MediaPlayer[] _players = new MediaPlayer[5];
    private float _waitTime = 2.0f;
    private float _timePassed;
    private readonly float _size = 100;

    private void Start()
    {
        _players[0] = mediaPlayerCNC;
        _players[1] = mediaPlayerWood;
        _players[2] = mediaPlayerMetal;
        _players[3] = mediaPlayerPaint;
        _players[4] = mediaPlayerTechniek;
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
        Debug.DrawRay(ray.origin, ray.direction * _size, Color.yellow);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit))
        {
            GameObject hitObject = hit.collider.transform.parent.gameObject;
            if (hitObject.name == "CNCCanvas") 
            {
                mediaPlayerCNC.Control.Play();
            }
            if (hitObject.name == "WOODCanvas")
            {
                mediaPlayerWood.Control.Play();
            }
            if (hitObject.name == "METALCanvas")
            {
                mediaPlayerMetal.Control.Play();
            }
            if (hitObject.name == "PAINTCanvas")
            {
                mediaPlayerPaint.Control.Play();
            }
            if (hitObject.name == "TECHNIEKCanvas")
            {
                mediaPlayerTechniek.Control.Play();
            }
            if (hitObject.name == "EXITCanvas")
            {
                _timePassed += Time.deltaTime;
                if (_timePassed >= _waitTime) 
                {
                    SceneManager.LoadScene("GANG_UNLOCKED", LoadSceneMode.Single);
                }
            }
        }
        else
        {
            _timePassed = 0f;
            for (int i = 0; i < _players.Length; i++)
            {
                _players[i].Control.Stop();
            }
        }
        float fillPercentage = _timePassed / _waitTime;
        fillImage.fillAmount = fillPercentage;
    }
}
