﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;

public class UpstairsSpeedUp : MonoBehaviour
{
    public MediaPlayer timelapse;

    private readonly float _regularSpeed = 1.0f;
    private float _currentSpeed;
    private float _changeSpeed;

    // Update is called once per frame
    void Update()
    {
        _currentSpeed = timelapse.Control.GetPlaybackRate();
        _changeSpeed = 0;
        _currentSpeed = Mathf.MoveTowards(_currentSpeed, _regularSpeed, 0.2f);
        if (OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger)) 
        {
            _changeSpeed = 2f;
        }
        _currentSpeed += _changeSpeed;
        timelapse.Control.SetPlaybackRate(_currentSpeed);
    }
}
