﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;

public class ChangeSphere : MonoBehaviour
{
    public GameObject playing;
    public GameObject enable;
    public MediaPlayer playingVideo;
    public MediaPlayer enableVideo;

    private readonly float _scale = 2f;

    // Start is called before the first frame update
    void Start()
    {
        playingVideo.Events.AddListener(OnMediaEvent);
    }

    public void OnMediaEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
    {
        switch (et)
        {
            case MediaPlayerEvent.EventType.FinishedPlaying:
                playing.transform.localScale = new Vector3(_scale, _scale, _scale);
                enable.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                enableVideo.Control.Play();
                playingVideo.Control.Stop();
                playingVideo.Control.SetLooping(false);
                break;
        }
    }
}
