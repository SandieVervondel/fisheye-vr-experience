﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneContentA : MonoBehaviour
{
    private float _waitTime = 90.0f;
    private float _timePassed;
    private int _random;

    private void Start()
    {
        _random = Random.Range(0, 2);
    }

    private void Update()
    {
        _timePassed += Time.deltaTime;
        if(_timePassed >= _waitTime && _random == 0) 
        {
            SceneManager.LoadScene("CONTENT_B", LoadSceneMode.Single);
        }
        if (_timePassed >= _waitTime && _random == 1)
        {
            SceneManager.LoadScene("GANG_UNLOCKED", LoadSceneMode.Single);
        }
    }
}
