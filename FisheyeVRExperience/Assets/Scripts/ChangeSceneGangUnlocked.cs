﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;
using UnityEngine.SceneManagement;

public class ChangeSceneGangUnlocked : MonoBehaviour
{
    public MediaPlayer scene;
    private int _randomNumber;

    // Start is called before the first frame update
    void Start()
    {
        scene.Events.AddListener(OnMediaEvent);
        _randomNumber = Random.Range(0, 3);
    }

    public void OnMediaEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
    {
        switch (et)
        {
            case MediaPlayerEvent.EventType.FinishedPlaying:
                if(_randomNumber == 0) 
                { 
                    SceneManager.LoadScene("CONTENT_A", LoadSceneMode.Single);
                }
                if (_randomNumber == 1)
                {
                    SceneManager.LoadScene("DESIGN", LoadSceneMode.Single);
                }
                if (_randomNumber == 2)
                {
                    SceneManager.LoadScene("ATELIER", LoadSceneMode.Single);
                }
                break;

        }
    }
}
