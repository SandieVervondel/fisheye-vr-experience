﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;
using UnityEngine.SceneManagement;

public class RaycastingGangUnlocked : MonoBehaviour
{
    public Image fillImageContent;
    public Image fillImageDesign;
    public Image fillImageAtelier;
    public Image fillImageUpstairs;
    public Image fillImageExit;

    private float _waitTime = 2.0f;
    private float _timePassed;
    private readonly float _size = 100;
    private Image _toFillImage;

    private void Start()
    {
        _toFillImage = fillImageContent;
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
        Debug.DrawRay(ray.origin, ray.direction * _size, Color.yellow);

        RaycastHit hit;
        if(Physics.Raycast(ray, out hit))
        {
            GameObject hitObject = hit.collider.transform.parent.gameObject;
            _timePassed += Time.deltaTime;
            if(hitObject.name == "CONTENT") 
            {
                _toFillImage = fillImageContent;
                if (_timePassed >= _waitTime)
                {
                    SceneManager.LoadScene("CONTENT_A", LoadSceneMode.Single);
                }
            }
            if (hitObject.name == "DESIGN")
            {
                _toFillImage = fillImageDesign;
                if (_timePassed >= _waitTime)
                {
                    SceneManager.LoadScene("DESIGN", LoadSceneMode.Single);
                }
            }
            if (hitObject.name == "ATELIER")
            {
                _toFillImage = fillImageAtelier;
                if (_timePassed >= _waitTime)
                {
                    SceneManager.LoadScene("ATELIER", LoadSceneMode.Single);
                }
            }
            if (hitObject.name == "UPSTAIRS")
            {
                _toFillImage = fillImageUpstairs;
                if (_timePassed >= _waitTime)
                {
                    SceneManager.LoadScene("UPSTAIRS", LoadSceneMode.Single);
                }
            }
            if (hitObject.name == "EXIT")
            {
                _toFillImage = fillImageExit;
                if (_timePassed >= _waitTime)
                {
                    SceneManager.LoadScene("START", LoadSceneMode.Single);
                }
            }
        }
        else
        {
            _timePassed = 0f;
            fillImageDesign.fillAmount = 0;
            fillImageAtelier.fillAmount = 0;
            fillImageContent.fillAmount = 0;
            fillImageUpstairs.fillAmount = 0;
            fillImageExit.fillAmount = 0;
        }
        float fillPercentage = _timePassed / _waitTime;
        _toFillImage.fillAmount = fillPercentage;
    }
}
