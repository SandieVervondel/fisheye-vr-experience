﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneContentB : MonoBehaviour
{
    private float _waitTime = 90.0f;
    private float _timePassed;

    private void Update()
    {
        _timePassed += Time.deltaTime;
        if(_timePassed >= _waitTime) 
        {
            SceneManager.LoadScene("CONTENT_A", LoadSceneMode.Single);
        }
    }
}
