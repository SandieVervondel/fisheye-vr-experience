﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;
using UnityEngine.SceneManagement;

public class ChangeSceneToGangVideo : MonoBehaviour
{
    public MediaPlayer scene;

    // Start is called before the first frame update
    void Start()
    {
        scene.Events.AddListener(OnMediaEvent);
    }

    public void OnMediaEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
    {
        switch (et)
        {
            case MediaPlayerEvent.EventType.FinishedPlaying:
                SceneManager.LoadScene("GANG_UNLOCKED", LoadSceneMode.Single);
                break;

        }
    }
}
