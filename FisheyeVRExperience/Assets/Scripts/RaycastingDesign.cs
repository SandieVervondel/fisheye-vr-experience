﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;
using UnityEngine.SceneManagement;

public class RaycastingDesign : MonoBehaviour
{
    public Image fillImageAnimation;
    public Image fillImageExit;
    public GameObject playing;
    public GameObject enable;
    public MediaPlayer animation;

    private float _waitTime = 2.0f;
    private float _timePassed;
    private readonly float _size = 100;
    private Image _toFillImage;
    private readonly float _scale = 2f;

    private void Start()
    {
        _toFillImage = fillImageAnimation;
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            GameObject hitObject = hit.collider.transform.parent.gameObject;
            _timePassed += Time.deltaTime;
            if (hitObject.name == "ANIMATION" && !animation.Control.IsPlaying())
            {
                _toFillImage = fillImageAnimation;
                if (_timePassed >= _waitTime)
                {
                    animation.Control.Rewind();
                    enable.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    playing.transform.localScale = new Vector3(_scale, _scale, _scale);
                    animation.Control.Play();
                }
            }
            if (hitObject.name == "EXIT")
            {
                _toFillImage = fillImageExit;
                if (_timePassed >= _waitTime)
                {
                    SceneManager.LoadScene("GANG_UNLOCKED", LoadSceneMode.Single);
                }
            }
        }
        else
        {
            _timePassed = 0f;
            fillImageAnimation.fillAmount = 0;
            fillImageExit.fillAmount = 0;
        }
        float fillPercentage = _timePassed / _waitTime;
        _toFillImage.fillAmount = fillPercentage;

    }
}
